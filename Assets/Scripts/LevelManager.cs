﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name) {
        //Debug.Log("Level load requested for " + name);
        SceneManager.LoadScene("Game");
    }

    public void FakeQuitRequest() {
        //Debug.Log("Quit requested");
        SceneManager.LoadScene("Bye");
    }

    public void QuitRequest() {
        //Debug.Log("Quit requested");
        Application.Quit();
    }

    public void BackToStart() {
        //Debug.Log("Quit requested");
        SceneManager.LoadScene("Start");
    }

    public void ComputerLoses() {
        //Debug.Log("Quit requested");
        SceneManager.LoadScene("Lose");
    }

    public void ComputerWins() {
        //Debug.Log("Quit requested");
        SceneManager.LoadScene("Win");
    }

    public void EmbarassYourself() {
        //Debug.Log("Quit requested");
        SceneManager.LoadScene("Derp");
    }
}
