﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class NumberWizard : MonoBehaviour {

    public Text number;
    public Text counter;

    int max = 1000;
    int min = 1;
    int currentGuess = 0;
    int maxGuesses = 10;

    bool computerWins = false;

    bool guessHigher = false;
    bool guessLower = false;
    bool noMoreGuesses = false;

    void Start() {
        StartCoroutine(NextGuess(min, max));
    }

    void Update() {
        if (maxGuesses < 0) {
            SceneManager.LoadScene("Lose");
        } else {
            number.text = string.Format("{0}", currentGuess);
            if (noMoreGuesses) {
                counter.text = "There are no more choices. Resistance is futile.";
            } else if (maxGuesses <= 0) {
                counter.text = "This MUST be it!!!";
            } else {
                counter.text = string.Format("I shall guess your number in no more than {0} tries!", maxGuesses);
            }
        }
    }

    public void GuessHigher() {
        guessHigher = true;
    }

    public void GuessLower() {
        guessLower = true;
    }

    public void ComputerWins() {
        computerWins = true;
    }

    IEnumerator NextGuess(int min, int max) {
        min = min <= this.max ? min : this.max; //due to guess adjustments they can go over max / under min, this is correction
        max = max >= this.min ? max : this.min;
        //print(string.Format("min: {0}, max: {1}, currentGuess:{2}", min, max, currentGuess));
        int guess = Random.Range(min, max + 1);
        currentGuess = guess;
        //print(string.Format("min: {0}, max: {1}, currentGuess:{2}, guessRemaining: {3}", min, max, currentGuess, maxGuesses));
        bool childRunning = false;
        while (!computerWins && !childRunning) {
            if (min != max) {
                //print(string.Format("on loop entry min: {0}, max: {1}", min, max));
                if (guessHigher) {
                    guessHigher = false;
                    --maxGuesses;
                    StartCoroutine(NextGuess(guess + 1, max)); //need to adjust guess, because don't want to guess same value again
                    childRunning = true; // don't watch for button presses if a recursive call already does so
                } else if (guessLower) {
                    guessLower = false;
                    --maxGuesses;
                    StartCoroutine(NextGuess(min, guess - 1));
                    childRunning = true;
                }
            } else {
                noMoreGuesses = true;
            }
            yield return null;
        }
        yield return null;
    }
}
